Application React Native Clément PETRE
J’ai décidé de faire une application météorologique, à l’aide de l’API CurrentWeatherData
développé par OpenWeatherMap :
https://openweathermap.org/ -- > https://openweathermap.org/current
Cette API sert à récupérer les donnes météorologiques en temps réel de villes, à l’échelle
mondiale. La localisation est récupérable par différents moyens, soit par le nom de la ville,
par le code postal ou encore par la latitude et la longitude.
Cette application contient plusieurs pages, donc un accueil résumant l’application, une page
servant à connaitre la position exacte (à l’aide de « expo-location »), une page contenant un
plugin de caméra, avec la possibilité de switch avant et arrière (réalisé avec « expocamera »).
La dernière page contient le retour de l’API météo et répertorie donc les données
météorologiques de villes qui sont prédéfinies dans le code source.