import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import axios from 'axios';

export default function Meteo(props) {

  const [data, setData] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => { 
    console.log();
    async function getData() {
        const result = await axios(
        'https://api.openweathermap.org/data/2.5/weather?q='+props.ville+'&appid=a8d85d49036be4ac98a674a11ca972a7',
        );
        setData(result.data);
        setIsLoading(false);
        console.log("test loading data");
        console.log(result.data);
        console.log(isLoading);
    }
    getData();
  } ,[]);

  return (
    <View style={styles.container}>
    <Text style={styles.paragraph}>
      Ville : {data.name}
    </Text>
    <Text style={styles.paragraph}>
        Température en °C: {Math.round(data.main.temp - 273.15)}
      </Text>
      <Text style={styles.paragraph}>
        TimeZone : {data.timezone}
      </Text>
      <Text style={styles.paragraph}>
        Visibilité en m : {data.visibility}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 24,
  },
  paragraph: {
    margin: 24,
    marginTop: 0,
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
  }
});
