import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Meteo from './Meteo';

export default function Meteo2() {
  return (
    <View style={styles.container}>
      <Meteo ville="Paris" />
      <Meteo ville="London" />
      <Meteo ville="Langeac" />
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#85c1e9',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
});